package si.uni_lj.fri.pbd.lab9;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.uni_lj.fri.pbd.lab9.models.dto.WeatherResponseDTO;
import si.uni_lj.fri.pbd.lab9.models.dto.WeatherResponsesDTO;
import si.uni_lj.fri.pbd.lab9.rest.RestAPI;
import si.uni_lj.fri.pbd.lab9.rest.ServiceGenerator;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener {
    private RestAPI mRestClient;
    private GoogleMap mMap;

    private ConnectivityManager mNwManager;
    private ConnectivityManager.NetworkCallback mNwCallback;
    private boolean mOnUnmetered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRestClient = ServiceGenerator.createService(RestAPI.class);

        mOnUnmetered = false;
        mNwCallback = new ConnectivityManager.NetworkCallback(){
            @Override
            public void onCapabilitiesChanged(@NonNull Network network, @NonNull NetworkCapabilities networkCapabilities) {
                super.onCapabilitiesChanged(network, networkCapabilities);
                if(networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_NOT_METERED)){
                    mOnUnmetered = true;
                }else{
                    mOnUnmetered = false;
                }
            }
        };

        mNwManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mNwManager.registerDefaultNetworkCallback(mNwCallback);





        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mNwManager.unregisterNetworkCallback(mNwCallback);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        //LatLng sydney = new LatLng(-34, 151);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom( new LatLng(46,14.5),8.0f) );
        mMap.setOnCameraIdleListener(this);
    }

    @Override
    public void onCameraIdle() {
        if(!mOnUnmetered){
            Toast.makeText(this,"Connect to an unmetered network, please",Toast.LENGTH_LONG).show();
            return;
        }
        LatLngBounds llb = mMap.getProjection().getVisibleRegion().latLngBounds;


        double lonLeft = llb.southwest.longitude;
        double latBot = llb.southwest.latitude;
        double lonRight = llb.northeast.longitude;
        double  latTop = llb.northeast.latitude;

        String bbox = lonLeft+","+latBot+","+lonRight+","+latTop+",10";
        //Log.d("llb",llb.toString());
        //Log.d("llb",bbox);

        mRestClient.getCurrentWeatherData(bbox,Constants.API_KEY)
                .enqueue(new Callback<WeatherResponsesDTO>() {
                    @Override
                    public void onResponse(Call<WeatherResponsesDTO> call, Response<WeatherResponsesDTO> response) {
                        Log.d("succ","succ");
                        if(response.isSuccessful()){
                            ArrayList<WeatherResponseDTO> wr = response.body().getWeatherResponses();
                            for(int i = 0; i < wr.size(); i++){
                                WeatherResponseDTO wrDTO = wr.get(i);

                                String name  = wrDTO.getName();

                                float lat = wrDTO.getCoord().getLatitude();
                                float lon = wrDTO.getCoord().getLongitude();

                                float temp = wrDTO.getMain().getTemp();


                                mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat,lon))
                                .title(name+" "+temp)
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<WeatherResponsesDTO> call, Throwable t) {
                        Log.d("failure","failure");
                    }
                });
    }
}
