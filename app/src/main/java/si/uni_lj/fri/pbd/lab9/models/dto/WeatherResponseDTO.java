package si.uni_lj.fri.pbd.lab9.models.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WeatherResponseDTO {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("coord")
    @Expose
    CoordDTO coord;

    @SerializedName("main")
    @Expose
    MainDTO main;

    @SerializedName("dt")
    private int dt;

    public int getId() { return id; }
    public String getName() { return name; }
    public int getDt() { return dt; }

    public void setId(int id) { this.id = id; }
    public void setName(String name) { this.name = name; }
    public void setDt(int dt) { this.dt = dt; }

    public CoordDTO getCoord(){
        return coord;
    }

    public MainDTO getMain(){
        return main;
    }

}
