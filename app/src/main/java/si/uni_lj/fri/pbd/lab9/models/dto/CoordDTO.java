package si.uni_lj.fri.pbd.lab9.models.dto;

import com.google.gson.annotations.SerializedName;

public class CoordDTO {

    @SerializedName("Lon")
    private float longitude;

    @SerializedName("Lat")
    private float latitude;

    public float getLongitude() { return longitude; }
    public float getLatitude() { return latitude; }

    public void setLongitude(float longitude) { this.longitude = longitude; }
    public void setLatitude(float latitude) { this.latitude = latitude; }

}
