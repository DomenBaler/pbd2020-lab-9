package si.uni_lj.fri.pbd.lab9.models.dto;

import com.google.gson.annotations.SerializedName;

public class MainDTO {
    @SerializedName("temp")
    private float temp;

    @SerializedName("temp_min")
    private float temp_min;

    @SerializedName("temp_max")
    private float temp_max;

    @SerializedName("pressure")
    private float pressure;

    @SerializedName("sea_level")
    private float sea_level;

    @SerializedName("grnd_level")
    private float grnd_level;

    public float getTemp() { return temp; }
    public float getTemp_min() { return temp_min; }
    public float getTemp_max() { return temp_max; }
    public float getPressure() { return pressure; }
    public float getSea_level() { return sea_level; }
    public float getGrnd_level() { return grnd_level; }

    public void setTemp(float temp) { this.temp = temp; }
    public void setTemp_min(float temp_min) { this.temp_min = temp_min; }
    public void setTemp_max(float temp_max) { this.temp_max = temp_max; }
    public void setPressure(float pressure) { this.pressure = pressure; }
    public void setSea_level(float sea_level) { this.sea_level = sea_level; }
    public void setGrnd_level(float grnd_level) { this.grnd_level = grnd_level; }
}
